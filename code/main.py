from scipy.sparse import csr_matrix
import betterspy
import numpy as np
from sklearn.cluster import KMeans
from scipy.sparse.linalg import eigsh, lobpcg
from scipy.sparse.csgraph import laplacian as conv_laplacian
import warnings
import plotly.graph_objects as go
import argparse
import sys
from pyamg import smoothed_aggregation_solver


parser = argparse.ArgumentParser()
parser.add_argument("--graph_name", type=str, help="GraphName, on which to run the analysis", default='oregon')
parser.add_argument("--use_amg", action="store_true", help="Lobcpg eigen solver with amg")
parser.add_argument("--verbose", action="store_true", help="Verbose mode")
parser.add_argument("--load_embedding", action="store_true", help="Loads the embedding of the graph from file")
parser.add_argument("--fine_tune", action="store_true", help="Fine tune best eigen decomposition only")
args = parser.parse_args()

warnings.filterwarnings('ignore')

class GraphPartitioner():
    def __init__(self, load_embedding = False, graph_name = 'ca', verbose = False):
        self.graph_id = 0
        self.num_of_vertices = 0
        self.num_of_edges = 0
        self.feature_space = 100
        self.k = 0
        self.load_embedding_ = load_embedding
        self.degs = None
        self.graph = None
        self.result = None
        self.laplacian = None
        self.verbose = verbose
        self.partition_distr = dict()
        self.edges = []
        self.file_names = dict(
            ca = 'ca-GrQc',
            oregon = 'Oregon-1',
            epi = 'soc-Epinions1',
            notredam = 'web-NotreDame',
            roadnet = 'roadNet-CA'
        )
        self.graph_name = graph_name
        self.num_of_iterations = dict(
            ca = 200,
            oregon = 200,
            epi = 40,
            notredam = 10,
            roadnet = 4
        )

    def visualize(self, data):
        x,y,z = np.hsplit(data, self.feature_space)
        layout = go.Layout(
            scene=dict(
                aspectmode='data'
            )
        )
        fig = go.Figure(data=[go.Scatter3d(
            x=x.flatten(),
            y=y.flatten(),
            z=z.flatten(),
            mode='markers',
            marker=dict(
                size=12,
                colorscale='Viridis',
                opacity=0.8
            )
        )], layout=layout)
        fig.show()

    def read_file(self):
        file = '../data/graphs/{}.txt'.format(self.file_names[self.graph_name])
        with open(file, 'r') as text_file:
            for i, line in enumerate(text_file):
                if i == 0:
                    _hastag, graph_id, num_of_vertices, num_of_edges, k = line.split(' ')
                    self.graph_id = graph_id
                    self.num_of_vertices = int(num_of_vertices)
                    self.num_of_edges = int(num_of_edges)
                    self.k = int(k[:-1])
                    self.graph = csr_matrix((self.num_of_vertices, self.num_of_vertices), dtype=np.int8)
                else:
                    v1, v2 = list(map(int, line.split(' ')))
                    self.graph[v1, v1] = 1
                    self.graph[v2, v2] = 1
                    self.graph[v1, v2] = 1
                    self.graph[v2, v1] = 1
                    self.edges.append([v1,v2])

    def save_embedding(self, embedding):
        np.save('../data/saved/eig_{}'.format(self.graph_id), embedding)

    def load_embedding(self):
        return np.load('../data/saved/eig_{}.npy'.format(self.graph_id))

    def get_embedding(self, use_amg = False, feature_space = 2):
        fallback = False
        if self.load_embedding_:
            embedding = self.load_embedding()
        else:
            random_state = np.random.RandomState(seed=0)
            n_components = feature_space + 1
            if self.laplacian is None:
                self.laplacian, self.degs = conv_laplacian(self.graph, normed=True, return_diag=True)
                if self.verbose:
                    betterspy.show(self.laplacian)
                    betterspy.write_png(
                        filename='../data/saved/lap_{}.png'.format(self.graph_id),
                        A=self.laplacian
                    )
            laplacian = self.laplacian.copy()
            degs = self.degs.copy()
            if use_amg or self.num_of_vertices >= 100000:
                try:
                    ml = smoothed_aggregation_solver(laplacian)
                    M = ml.aspreconditioner()
                    X = random_state.rand(laplacian.shape[0], n_components + 1)
                    X[:, 0] = degs.ravel()
                    _eigen_values, eigen_vectors = lobpcg(
                        laplacian, 
                        X, 
                        M=M, 
                        tol=1.e-11, 
                        maxiter=5000,
                        largest=False
                    )
                    embedding = eigen_vectors.T
                except Exception:
                    fallback = True
                    
            if (not use_amg and self.num_of_vertices < 100000) or fallback:
                laplacian *= -1
                v0 = random_state.uniform(-1, 1, laplacian.shape[0])
                _eigen_values, eigen_vectors = eigsh(
                    laplacian, 
                    k=n_components,
                    v0=v0,
                    sigma=1.0,
                    which='LM'
                )
                embedding = eigen_vectors.T[n_components::-1]

            embedding /= degs

            max_abs_rows = np.argmax(np.abs(embedding), axis=1)
            signs = np.sign(embedding[range(embedding.shape[0]), max_abs_rows])
            embedding *= signs[:, np.newaxis]

            embedding = embedding[1:n_components].T
        return embedding

    def optimize(self, use_amg = False, fine_tune = False):
        init_value = max(2, self.k - 2)
        num_of_iterations = self.num_of_iterations[self.graph_name]
        delta = 220/num_of_iterations

        best_score = sys.maxsize
        for i in range(num_of_iterations):
            self.partition_distr.clear()
            feature_space = int(init_value + delta * i)

            max_iter = 1500 if fine_tune else 750
            embedding = self.solve(use_amg, feature_space, max_iter)
            score = self.score_function()

            file = open('../results/{}_scores.txt'.format(self.graph_id), 'a+')
            file.write('Feature space: {}, Score: {}\n'.format(feature_space, score))
            file.close()
            print('Feature space: {}, score: {}, Best score: {}'.format(feature_space, score, best_score))
            if score <= best_score:
                best_score = score
                self.save_solution()
                if not fine_tune:
                    self.save_embedding(embedding)

    def save_solution(self):
        with open('../results/{}.output'.format(self.graph_id), 'w') as text_file:
            print('# {} {} {} {}'.format(self.graph_id, self.num_of_vertices, self.num_of_edges, self.k), file=text_file)
            for node_id, partition in enumerate(self.result):
                self.partition_distr[node_id] = partition
                print('{} {}'.format(node_id, partition), file=text_file)
                self.partition_counter[partition] += 1
            if self.verbose:
                print(self.partition_counter)

    def solve(self, use_amg = False, feature_space = 3, max_iter = 750):
        embedding = self.get_embedding(use_amg, feature_space)
        k_means = KMeans(
            n_clusters = self.k,
            n_jobs=-1,
            n_init=25,
            random_state=0,
            max_iter=max_iter,
            algorithm='elkan'
        )
        k_means.fit(embedding)
        if self.verbose:
            print(k_means.cluster_centers_)
        
        self.result = k_means.predict(embedding)
        if self.verbose:
            print(self.result)
        self.partition_counter = [0 for i in range(self.k)]
        for node_id, partition in enumerate(self.result):
            self.partition_distr[node_id] = partition
            self.partition_counter[partition] += 1
        return embedding

    def score_function(self):
        out_edge_counter = [0 for i in range(self.k)]
        for v1, v2 in self.edges:
            part1, part2 = self.partition_distr[v1], self.partition_distr[v2]
            if (part1 != part2):
                out_edge_counter[part1] += 1
                out_edge_counter[part2] += 1
        score = 0
        for i in range(len(self.partition_counter)):
            single_score = out_edge_counter[i] / float(self.partition_counter[i])
            score += single_score
        score = round(score, 4)
        return score

if __name__ == "__main__":
    args = parser.parse_args()
    load_embedding = args.load_embedding or args.fine_tune
    gp = GraphPartitioner(
        load_embedding, 
        args.graph_name, 
        args.verbose
    )
    gp.read_file()
    gp.optimize(args.use_amg, args.fine_tune)

