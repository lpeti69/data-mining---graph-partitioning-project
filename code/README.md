# Data Mining - Graph Partitioning Project

## Run

To reproduce the results one has to run the following code:
```cd code```
```
python3 main.py 
    --graph_name [GRAPH_NAME] : name of the graph (based on our dictionary), on which to run the analysis
    --use_amg : Use it to run the eigen decomposition with Lobcpg eigen solver using amg preconditioner
    --verbose : Print out intermediate results
    --load_embedding : Load a previously computed embedding of the specified graph
    --fine_tune : Search for the embedding space dimenion with the the best score based on the objective function
```
where providing the graph_name is necessary, the others are optional.